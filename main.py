from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5 import uic
import serial
import serial.tools.list_ports
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtCore import pyqtSignal
from SerialReadThread import SerialReadThread


UI = uic.loadUiType("MainWindow.ui")[0]

class SerialMonitor(QtWidgets.QMainWindow, UI):
    LEN_CYCLE = 5  # 15
    SPLIT_LITERAL = ','
    received_data = pyqtSignal(str, name="receivedData")

    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.setup()
        self.id_list = {}
        self.buf = []
        self.dev = None
        self.thread = None
    
    def setup(self):
        self.comboBox_Baudrate.addItems(['1200','2400', '4800', '9600', '19200', '38400', '57600', '115200'])
        self.comboBox_Baudrate.setCurrentIndex(4)
        self.comboBox_Data.addItems(['5','6','7','8'])
        self.comboBox_Data.setCurrentIndex(3)
        self.comboBox_Flow.addItems(['None'])
        self.comboBox_Parity.addItems(['N'])
        self.comboBox_Stop.addItems(['1', '2'])
        self.comboBox_Port.addItem('COM20')
        
        self.pushButton_Reload.clicked.connect(self.btnReloadClicked)
        self.pushButton_Connect.clicked.connect(self.btnConncectClicked)
        self.toolButton_2.clicked.connect(self.btnFDialogClicked)

    def btnConncectClicked(self):
        if self.pushButton_Connect.isChecked() :
            if self.lineEdit.text() != "":
                if self.OpenPort():
                    self.StartThread()
                    self.getSensorIDs()
            else:   
                QtWidgets.QMessageBox.warning(self, 'Warning', 'Blank File Directory for Logging    ')
                self.pushButton_Connect.toggle()

        else:
            self.ClosePort()
            self.StopThread()
    
    def btnReloadClicked(self):
        self.LoadPorts()

    def btnFDialogClicked(self):
        self.GetFileName()

    def btnInsertClicked(self):
        print("insert")

    def StartThread(self):
        self.thread = SerialReadThread(self.dev)
        self.thread.received_data.connect(self.procData)
        self.thread.start()
    
    def StopThread(self):
        self.thread.quit()

    def OpenPort(self): 
        try:
            self.dev = serial.Serial(port=self.comboBox_Port.currentText(),
                            baudrate=int(self.comboBox_Baudrate.currentText()),
                            bytesize=int(self.comboBox_Data.currentText()),
                            parity=self.comboBox_Parity.currentText(),
                            stopbits=float(self.comboBox_Stop.currentText()))
            
            #self.dev.open()
            self.textEdit.append("Serial Port Connected")
            return True
        except:
            self.textEdit.append("Serial Port Connection Failed")
            QtWidgets.QMessageBox.warning(self, 'Warning', 'Please Check Serial Configuration    ')
            self.pushButton_Connect.toggle()
            return False


    def ClosePort(self):
        if self.dev.isOpen():
            self.dev.close()
        self.dev = None
        self.textEdit.append("Serial Port Closed")

    def LoadPorts(self):
        self.comboBox_Port.clear()
        ports = serial.tools.list_ports.comports()

        available_ports = []
        for p in ports:
            available_ports.append(p.device)
        if len(available_ports) > 0 :
            available_ports.sort()
            for port in available_ports:
                self.comboBox_Port.addItem(port)

    def GetFileName(self):
        fname = QtWidgets.QFileDialog.getOpenFileName(self, 'Open File', '.')
        if fname[0]:
            self.lineEdit.setText(fname[0])
                
    @pyqtSlot(str)
    def procData(self, data):
        self.textEdit.append(data)
        self.saveData(data)
        self.checkData(data)

    def getSensorIDs(self):
        self.id_list['Head'] = self.lineEdit_Head.text()
        self.id_list['Chest'] = self.lineEdit_Chest.text()
        self.id_list['LH'] = self.lineEdit_LH.text()
        self.id_list['RH'] = self.lineEdit_RH.text()
        self.id_list['LF'] = self.lineEdit_LF.text()
        self.id_list['RF'] = self.lineEdit_RF.text()

        for val in self.id_list.values():
            if val == None or val == "":
                return QtWidgets.QMessageBox.warning(self, 'Warning', 'Blank Sensor Id')


    def saveData(self, buf):
        f = open(self.lineEdit.text(), 'a')
        f.write(buf)
        f.close()

    def checkData(self, buf):
        if len(self.buf) > self.LEN_CYCLE:
            self.checkSensors()

        data = buf.split(self.SPLIT_LITERAL)
        self.buf.append(data[0])
        
    def checkSensors(self):
        bSensor = {}

        for part, id in self.id_list.items():
            print(part)
            if id in self.buf:
                bSensor[part] = True
            else:
                bSensor[part] = False
        self.updateSensorsState(bSensor)
        self.buf.clear()

    def updateSensorsState(self, sensorState):
        totalState = True
        strNW = ''

        if sensorState['Head'] is True:
            self.checkBox_Head.setCheckState(True)
        else:
            self.checkBox_Head.setCheckState(False)
            totalState = False
            strNW += 'Head '


        if sensorState['Chest'] is True:
            self.checkBox_Chest.setCheckState(True)
        else:
            self.checkBox_Chest.setCheckState(False)
            totalState = False
            strNW += 'Chest '


        if sensorState['LH'] is True:
            self.checkBox_LH.setCheckState(True)
        else:
            self.checkBox_LH.setCheckState(False)
            totalState = False
            strNW += 'LH '

        if sensorState['RH'] is True:
            self.checkBox_RH.setCheckState(True)
        else:
            self.checkBox_RH.setCheckState(False)
            totalState = False
            strNW += 'RH '

        if sensorState['LF'] is True:
            self.checkBox_LF.setCheckState(True)
        else:
            self.checkBox_LF.setCheckState(False)
            totalState = False
            strNW += 'LF '

        if sensorState['RF'] is True:
            self.checkBox_RF.setCheckState(True)
        else:
            self.checkBox_RF.setCheckState(False)
            totalState = False
            strNW += 'RF '

        if totalState is True:
            self.checkBox_Total.setCheckState(True)
        else:
            self.checkBox_Total.setCheckState(False)
            self.textEdit.append(strNW+"not Working")

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    monitor = SerialMonitor()
    monitor.show()
    app.exec_()
