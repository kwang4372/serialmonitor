from PyQt5.QtCore import QThread
import time
from PyQt5.QtCore import QWaitCondition
from PyQt5.QtCore import QMutex
from PyQt5.QtCore import pyqtSignal


class SerialReadThread(QThread):
    received_data = pyqtSignal(str, name="receivedData")

    def __init__(self, serial):
        QThread.__init__(self)
        self.cond = QWaitCondition()
        self._status = False
        self.mutex = QMutex()
        self.ser = serial

    def __del__(self):
        self.wait()

    def run(self):
        while True:
            # self.ser.write(b'th')
            self.mutex.lock()

            if self.ser.isOpen():
                buf = self.ser.readline().decode()
                if buf:
                    self.received_data.emit(buf)
            time.sleep(0.1)
            self.mutex.unlock()